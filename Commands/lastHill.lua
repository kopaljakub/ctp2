function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.running=false
end

function Run(self, units, parameter)
	self.threshold = 100

	local listOfUnits = parameter.listOfUnits -- table
	   -- local position = parameter.position -- Vec3
	
	if not self.running then
		SpringGiveOrderToUnit(listOfUnits[1], CMD.LOAD_UNITS, { 300, 0, 250, 250 }, {"shift"})
		SpringGiveOrderToUnit(listOfUnits[1], CMD.UNLOAD_UNITS, {3755,0,240,50}, {"shift"})
		self.running = true
	end

	return RUNNING

	
end


function Reset(self)
	ClearState(self)
end
