function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
end

function Run(self, units, parameter)
	self.threshold = 100

	local listOfUnits = parameter.listOfUnits -- table
   	-- local position = parameter.position -- Vec3

	if self.pos == nil then
		self.pos = {}
		if #listOfUnits > 0 then
			self.pos[1] = Vec3(1375, 0, 280)
			SpringGiveOrderToUnit(listOfUnits[1], CMD.MOVE, self.pos[1]:AsSpringVector(), {})
		end
		if #listOfUnits > 1 then
			self.pos[2] = Vec3(1950, 0, 780)
			SpringGiveOrderToUnit(listOfUnits[2], CMD.MOVE, self.pos[2]:AsSpringVector(), {})
		end
		if #listOfUnits > 2 then
			self.pos[3] = Vec3(2600, 0, 340)
			SpringGiveOrderToUnit(listOfUnits[3], CMD.MOVE, self.pos[3]:AsSpringVector(), {})
		end
		for i=4, #listOfUnits do
			self.pos[i] = Vec3(3730, 0, 220)
			SpringGiveOrderToUnit(listOfUnits[i], CMD.ATTACK, self.pos[i]:AsSpringVector(), {})
		end
	end

	for i=1, #listOfUnits do
		local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[i]) 
		local pointmanPosition = Vec3(pointX, pointY, pointZ)
		if (pointmanPosition:Distance(self.pos[i]) > self.threshold) then
			return RUNNING
		end
	end
	
	
	return SUCCESS
	
end


function Reset(self)
	ClearState(self)
end
